import { createRouter, createWebHistory } from 'vue-router'

import NewsList from '@/pages/News/NewsList.vue';
import NewsDetail from '@/pages/News/NewsDetail.vue';
import News from "@/pages/News/News.vue";

const routes = [
    {
        path: '/',
        name: 'News',
        component: News,
        children: [
            {
                path: '',
                name: 'NewsList',
                component: NewsList,
            },
            {
                path: ':slug',
                name: 'NewsDetail',
                component: NewsDetail,
            },
        ],
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router;
