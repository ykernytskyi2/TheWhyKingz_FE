import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import 'bootstrap/dist/css/bootstrap.min.css'
import router from './router'
import api from "./api/api.js";

const app = createApp(App)
app.use(router)
app.config.globalProperties.$http = api

app.mount('#app')
